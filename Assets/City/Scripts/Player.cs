﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public Transform PlayerTransform;
	private void Update ()
	{
	    //PlayerTransform.rotation = Quaternion.Euler(0,transform.rotation.y,0);
		var speed = Time.deltaTime * 10f;
		
		if(Input.GetAxis("Vertical") > 0)
			PlayerTransform.position += transform.forward * speed;

        if (Input.GetAxis("Vertical") < 0)
            PlayerTransform.position -= transform.forward * speed;

	    if (Input.GetAxis("Horizontal") < 0)
            PlayerTransform.Translate(Vector3.left * speed, this.transform);

        if (Input.GetAxis("Horizontal") > 0)
            PlayerTransform.Translate(Vector3.right * speed, this.transform);
    }
}
