﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PhysioAdapter : MonoBehaviour {

    private bool _calcMean = true;
    private float _timer = 60.0f;
    private float _messageTime = 4.0f;
    private float _messageTimer = 30.0f;
    private int _mean;
    public static int Threshold = 0;
    private List<int> _hrCaptured = new List<int>();
    private bool _calculated = false;
    private bool _canReleaseMessages;

    public GameObject Message;
    public Text MessageLeft, MessageRight;
	
    private void Start()
    {
        InvokeRepeating("AddToList", 1f, 1.0f);
        Message.SetActive(false);
    }

	void Update ()
    {
	    if(_calcMean)
        {
            _timer -= Time.deltaTime;
            if(_timer<0)
            {
                CancelInvoke("AddToList");
                CalculateMean();
                _calcMean = false;
            }
        }

        if(_calculated)
        {
            if (SensorData.HeartRate < Threshold)
            {
                Debug.Log("Bellow threshold");
                TherapistControl.CatTrigger = true;
                Message.SetActive(false);
            }
            else
            {
                _messageTimer -= Time.deltaTime;
                if(_messageTimer < 0)
                {
                    Message.SetActive(true);
                    if(SensorData.HeartRate > _mean)
                    {
                        MessageLeft.text = "Threshold: " + Threshold + ".   Counting your breath can help you relax";
                        MessageRight.text = "Threshold: " + Threshold + ".   Counting your breath can help you relax";
                    }
                    else
                    {
                        MessageLeft.text = "Threshold: " + Threshold + ".   You're doing good, keep relaxing";
                        MessageRight.text = "Threshold: " + Threshold + ".   You're doing good, keep relaxing";
                    }

                    _messageTime -= Time.deltaTime;
                    if(_messageTime < 0)
                    {
                        Message.SetActive(false);
                        _messageTimer = 30.0f;
                        _messageTime = 4.0f;
                    }
                }
            }
        }
	}

    private void AddToList()
    {
        //Debug.Log("Saving HR" + SensorData.HeartRate);
        _hrCaptured.Add(SensorData.HeartRate);
    }

    private void CalculateMean()
    {
        if (!_calculated)
        {
            for (var i = 0; i < _hrCaptured.Count; i++)
            {
                _mean += _hrCaptured[i];
            }
            _mean = (int)_mean / _hrCaptured.Count;
            Threshold = (int) (_mean - (_mean * 0.05f));
            Debug.Log("threshold: " + Threshold);
            _calculated = true;
        }
    }
}
