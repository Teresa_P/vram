﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HeartAnim : MonoBehaviour
{
    public static float BeatTime = 1.0f;
    //public Text HeartRateText;
    
	private void Update ()
    {
        BeatTime = SensorData.HeartRate / 60;
        var anim = GetComponent<Animation>();
        foreach (AnimationState state in anim)
        {
            state.speed = BeatTime;
        }
	    //HeartRateText.text = Math.Round(BeatTime*60).ToString("0");
    }

    
}
