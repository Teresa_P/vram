﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UiNavigation : MonoBehaviour {

    public GameObject Intro, CloseBtn, TopImage, Panel1, Panel2, Panel3, PanelOver;
    public GameObject Port1, Port2;
    public Toggle Panel1Toggle, Panel2Toggle;
    private float _timer = 3.0f;
    public static bool GameOver = false;
    public GameObject UDPprefab;
    private bool _firstScreenSet;
   
    private void Start()
    {
        var udp = GameObject.FindGameObjectWithTag("UDP");

        if (udp == null)
            Instantiate(UDPprefab);

        _firstScreenSet = false;
    }

	
	// Update is called once per frame
	void Update ()
    {
        if(UDPData.UDPset && !_firstScreenSet)
        {
            if (!GameOver)
                SetInterface(0);
            else
            {
                SetInterface(4);
                GameOver = false;
            }

            _firstScreenSet = true;
        }

	    if(Intro.activeSelf)
        {
            _timer -= Time.deltaTime;
            if(_timer <0)
            {
                SetInterface(1);
            }
        }

        if (UDPData.UDPset)
        {
            Panel1Toggle.gameObject.GetComponentInChildren<Text>().text = Panel1Toggle.isOn ? "Disconnect" : "Connect";

            Panel2Toggle.gameObject.GetComponentInChildren<Text>().text = Panel2Toggle.isOn ? "Disconnect" : "Connect";
        }
    }

    public void SetInterface(int step)
    {
        if(step == 0)
        {
            Intro.SetActive(true);
            Intro.SetActive(true);
            CloseBtn.SetActive(false);
            TopImage.SetActive(false);
            Panel1.SetActive(false);
            Panel2.SetActive(false);
            Panel3.SetActive(false);
            PanelOver.SetActive(false);
        }
        else if (step == 1)
        {
            Intro.SetActive(false);
            CloseBtn.SetActive(true);
            TopImage.SetActive(true);
            Panel1.SetActive(true);
            Port1.SetActive(false);
            Panel2.SetActive(false);
            Panel3.SetActive(false);
            PanelOver.SetActive(false);
        }
        else if (step == 2)
        {
            Intro.SetActive(false);
            CloseBtn.SetActive(true);
            TopImage.SetActive(true);
            Panel1.SetActive(false);
            Panel2.SetActive(true);
            Port2.SetActive(false);
            Panel3.SetActive(false);
            PanelOver.SetActive(false);
        }
        else if (step == 3)
        {
            Intro.SetActive(false);
            CloseBtn.SetActive(true);
            TopImage.SetActive(true);
            Panel1.SetActive(false);
            Panel2.SetActive(false);
            Panel3.SetActive(true);
            PanelOver.SetActive(false);
        }
        else if (step == 4)
        {
            Intro.SetActive(false);
            CloseBtn.SetActive(true);
            TopImage.SetActive(false);
            Panel1.SetActive(false);
            Panel2.SetActive(false);
            Panel3.SetActive(false);
            PanelOver.SetActive(true);
        }
    }

    public void Panel1Settings()
    {
        Port1.SetActive(!Port1.activeSelf);
    }

    public void Panel2Settings()
    {
        Port2.SetActive(!Port2.activeSelf);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
