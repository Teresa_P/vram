/*
    -----------------------
    based on 

    UDP-Receive
    -----------------------
     [url]http://msdn.microsoft.com/de-de/library/bb979228.aspx#ID0E3BAC[/url]
*/

using UnityEngine;
using System;
using System.Globalization;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine.UI;

public class UDPReceive : MonoBehaviour {
    
    private InputField _portInputField;
    private Text _adressLabelText;
    private Toggle _connect;
	private Text _messageTA, _messageHR, _messageE01, _messageE02;

    private Thread _receiveThread; 
    private UdpClient _client;
    private bool _isConnected;
 
    private int _port; // define > init
	public static string _portField = "11111";
	public static bool Flag;
    
    public string LastReceivedUDPPacket = "";
    public string AllReceivedUDPPackets = ""; 
	
	private string _text = "";
    
    private bool _first = true;
   
    public void Start()
    {
        _portInputField = GameObject.Find("PortInputField").GetComponent<InputField>();
        _adressLabelText = GameObject.Find("IP").GetComponent<Text>();
        _connect = GameObject.Find("ConnectToggle").GetComponent<Toggle>();
        _messageTA = GameObject.Find("MessageTA").GetComponent<Text>();
        _messageHR = GameObject.Find("MessageHR").GetComponent<Text>();
        _messageE01 = GameObject.Find("MessageE01").GetComponent<Text>();
        _messageE02 = GameObject.Find("MessageE02").GetComponent<Text>();

        DontDestroyOnLoad(gameObject);
    }

    public void init()
    {	
        // Create a new thread to receive incoming messages.
        _receiveThread = new Thread(new ThreadStart(ReceiveData));
        _receiveThread.IsBackground = true;
        _receiveThread.Start();
        _isConnected = true;
    }
 
    // receive thread 
    private void ReceiveData() 
    {
        _client = new UdpClient(_port);

        while (_isConnected) 
        {
            try 
            { 
                // receive Bytes from 127.0.0.1
				IPEndPoint anyIP = new IPEndPoint(IPAddress.Loopback, 0);

        	    byte[] data = _client.Receive(ref anyIP);

                //  UTF8 encoding in the text format.
			    _text = Encoding.UTF8.GetString(data);
                Debug.Log("text: " + _text);
                // latest UDPpacket
                LastReceivedUDPPacket = _text; 
				
				
        
				
				//	[$]<data  type> , [$$]<device> , [$$$]<joint> , <transformation> , <param_1> , <param_2> , .... , <param_N>
						
				string[] separators = {"[$]","[$$]","[$$$]",",",";"," "};
			      
			    string[] words = _text.Split(separators, StringSplitOptions.RemoveEmptyEntries);

                for (var i = 0; i < words.Length; i++)
                {
                     if (words[i] == "hr")
                    {
                        SensorData.HeartRate = int.Parse(words[i + 1]);
                    }
                    else if (words[i] == "element01")
                    {
                        SensorData.Element01 = float.Parse(words[i + 1]);
                    }
                    else if (words[i] == "element02")
                    {
                        SensorData.Element02 = float.Parse(words[i + 1]);
                    }
                    else if (words[0] == "catTrigger")
                    {
                        TherapistControl.CatTrigger = bool.Parse(words[i + 1]);
                    }

                }
                
                AllReceivedUDPPackets = AllReceivedUDPPackets + _text;  
            }
            catch (Exception err) 
            {
                print(err.ToString());
            }
        }
    }
	
	
void Update () 
	{
        if (Application.loadedLevel == 0)
        {
            _adressLabelText.text = "Your IP is " + LocalIPAddress();
            _portField = _portInputField.text;

            _messageTA.text = "Cat Trigger: " + TherapistControl.CatTrigger.ToString();
            _messageHR.text = "HR: " + SensorData.HeartRate.ToString();
            _messageE01.text = "Elem01: " + SensorData.Element01.ToString(CultureInfo.InvariantCulture);
            _messageE02.text = "Elem02: " + SensorData.Element02.ToString(CultureInfo.InvariantCulture);

            if (_connect.isOn)
            {
                if (_first)
                {
                    _first = false;
                    _port = int.Parse(_portField);
                    init();
                    Flag = true;
                    print("Start Server");
                    Handheld.Vibrate();
                }
            }
            else
            {
                if (!_first)
                {
                    _first = true;
                    Flag = false;
                    _receiveThread.Abort();
                    _client.Close();
                    print("Stop Server");
                    Handheld.Vibrate();
                }
            }
        }
	}	
	
	//get local ip address
	public string LocalIPAddress()
	{
	    IPHostEntry host;
	    string localIP = "";
	    host = Dns.GetHostEntry(Dns.GetHostName());
	    foreach (IPAddress ip in host.AddressList)
	    {
            if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork )
                localIP = ip.ToString();
	    }

	    return localIP;
	}
	
    // getLatestUDPPacket
    // cleans up the rest
    public string getLatestUDPPacket()
    {
        AllReceivedUDPPackets="";
        return LastReceivedUDPPacket;
    }

	void OnDisable() 
	{ 
	    if (_receiveThread!= null)
		{
            _isConnected = false;
            _receiveThread.Abort();
		    _client.Close();
		}
	 } 
	
	void OnApplicationQuit () 
	{
		if (_receiveThread!= null)
		{
            _isConnected = false;
            _receiveThread.Abort(); 
		    _client.Close();
		}
	}
}