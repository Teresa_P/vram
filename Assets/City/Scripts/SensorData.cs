﻿using UnityEngine;
using System.Collections;
using System;
using System.Globalization;
using System.IO;
using UnityEngine.UI;

public class SensorData : MonoBehaviour
{
    //public int TestHR = 60;
    public static int HeartRate;
    public static float Element01, Element02;
    public Text LeftHr, LeftE01, LeftE02;
    public Text RightHr, RightE01, RightE02;

    private void Update()
    {
        //HeartRate = TestHR;
        LeftHr.text = HeartRate.ToString();
        RightHr.text = HeartRate.ToString();
        LeftE01.text = Element01.ToString(CultureInfo.InvariantCulture);
        RightE01.text = Element01.ToString(CultureInfo.InvariantCulture);
        LeftE02.text = Element02.ToString(CultureInfo.InvariantCulture);
        RightE02.text = Element02.ToString(CultureInfo.InvariantCulture);
    }

}
