﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Cat : MonoBehaviour
{
    public GameObject Player, Profile, Particles;
    public static bool Found;
    private float _profileTimer = 10.0f;
    public static float Distance;
    private AudioSource Meow;
    private bool _soundDistSet;

    private void Start()
    {
        Meow = gameObject.GetComponentInChildren<AudioSource>();
    }

    private void Update()
    {
        Distance = Vector3.Distance(transform.position, Player.transform.position);
        //Debug.Log(Distance + Found.ToString());
        if (Found)
        {
            _profileTimer -= Time.deltaTime;

            if(_profileTimer < 6.0f)
                Profile.SetActive(true);

            if (_profileTimer < 0)
            {
                Profile.SetActive(false);
                Found = false;
                _profileTimer = 10.0f;
                UiNavigation.GameOver = true;
                SceneManager.LoadScene(0);
            }
        }

        if(!_soundDistSet && TherapistControl.CatTrigger && Distance >= 75)
        {
            Meow.maxDistance = Distance + 5;
            _soundDistSet = true;
        }
    }

    public void CatFound()
    {
        Particles.SetActive(true);
        //Debug.Log("Trigered");
        if (Distance < 30 && TherapistControl.CatTrigger)
        {
            Found = true;
        }
    }

    public void PointerExit()
    {
        if (!Profile.activeSelf)
        {
            Found = false;
            _profileTimer = 10.0f;
            
        }
        Particles.SetActive(false);
    }
}
