﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	public void LaunchEnvironment(int scene)
    {
        SceneManager.LoadScene(scene);
    }
}
