﻿using UnityEngine;
using System.Collections;

public class TherapistControl : MonoBehaviour
{

    public static bool CatTrigger = false;
    private GameObject _catObject = null;
    private bool _catSet = false;

    void Update ()
    {
	    if (Application.loadedLevel == 1 && _catObject == null && !_catSet)
	    {
	        _catObject = GameObject.FindGameObjectWithTag("Cat");
            _catObject.SetActive(false);
            _catSet = true;
	    }
        UDPData.SendString("distance," + Cat.Distance + ",catFound," + Cat.Found + ",hr," + SensorData.HeartRate + ",element01," + SensorData.Element01 + ",element02," + SensorData.Element02 + ",threshold," + PhysioAdapter.Threshold + ",catTrigger," + CatTrigger);

	    if (CatTrigger && _catObject != null)
	    {
            _catObject.SetActive(true);
            
	    }
    }
}
