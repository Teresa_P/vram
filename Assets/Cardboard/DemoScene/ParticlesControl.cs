﻿using UnityEngine;
using System.Collections;

public class ParticlesControl : MonoBehaviour {

    public static float ParticlesSize = 0.2f;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        GetComponent<ParticleSystem>().startSize = ParticlesSize;
    }
}
